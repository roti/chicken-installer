@echo off
setlocal

if "%1"=="" goto print_help

set CHICKEN_VERSION=%1
set CHICKEN_MAJOR=%CHICKEN_VERSION:~0,5%

echo Starting build for chicken version %CHICKEN_VERSION%

set URL=http://code.call-cc.org/releases/%CHICKEN_MAJOR%/chicken-%CHICKEN_VERSION%.tar.gz
:: figure out how to make this automatic
set CHICKEN_DIR=chicken-%CHICKEN_VERSION%

set BUILD_DIR=%~dp0
set SRC_DIR=%BUILD_DIR%src
set BIN_DIR=%BUILD_DIR%bin
set CURRENT_DIR=%CD%

set PATH=%BUILD_DIR%mingw\bin;%BUILD_DIR%tools;%PATH%


:cleanup
if exist "%SRC_DIR%" (rmdir /S /Q "%SRC_DIR%" || goto error)
if exist "%BIN_DIR%" (rmdir /S /Q "%BIN_DIR%" || goto error)
mkdir "%SRC_DIR%" || goto error
cd "%SRC_DIR%" || goto error

:download
echo Downloading %URL%
powershell -Command "& {param($a,$f) (new-object System.Net.WebClient).DownloadFile($a, $f)}" ""%URL%"" ""chicken.tar.gz"" || goto error


:extract
bsdtar -xzf chicken.tar.gz || goto error

:patch
cd "%CHICKEN_DIR%"
::This pach is needed only for the 4.8.x releases
if "%CHICKEN_VERSION:~0,3%"=="4.8" (echo Applying patch for 4.8.x releases && patch setup-api.scm < "%BUILD_DIR%setup-api.scm.patch" || goto error)


:build
mingw32-make PLATFORM=mingw PREFIX=c:/chicken || goto error
:: TODO check existence of c:\chicken
mingw32-make PLATFORM=mingw PREFIX=c:/chicken install || goto error

mkdir "%BIN_DIR%" || goto error
xcopy /E c:\chicken "%BIN_DIR%" || goto error
rmdir /S /Q c:\chicken || goto error

:: The End
goto :eof

:print_help
echo *** Error occured: chicken version number missing.
echo Usage example:
echo 	%0 4.8.0.3
goto :eof

:error
echo *** Error occured. Aborting.
goto :eof
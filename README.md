# Chicken Installer

A Windows installer for [Chicken Scheme](http://call-cc.org/).

Works with Chicken 4.8.0.x and later, and includes MinGW GCC Compiler 4.7.2. Only 32-bit builds are suported.

The installer has tested only on Windows 7 64-bit. Please let me know if it workes fine on other systems as well.

TODOs:

- Make installation of GCC compiler optional.
- Add GCC license
- Add openssl library
- Add [gtk](http://www.gtk.org/download/win32.php)

## Build Instructions

To build an installer you need [NSIS](http://nsis.sourceforge.net/). The [Unicode version](http://www.scratchpaper.com/) is highly recommended because the standard NSIS compiler limits strings to 1024 characters, which causes problems when Chicken is added to PATH.

Steps to build an installer:

1. Run build.bat with the chicken version as parameter.

		D:\projects\chicken-installer>build.bat 5.2.0

	The script will download the sources and build Chicken. Resulting binaries are placed in the bin folder. The script uses c:\chicken as a staging folder, so make sure it doesn't exist already.

2. Open chicken-installer.nsi with your favourite text editor and set the correct values for variables VERSIONMAJOR, VERSIONMINOR, VERSIONBUILD and INSTALLSIZE.

		!define VERSIONMAJOR 5
		!define VERSIONMINOR 2
		!define VERSIONBUILD 0

3. Build chicken-installer.nsi using the NSIS Compiler.
